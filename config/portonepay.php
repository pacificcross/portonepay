<?php
 /*
    |--------------------------------------------------------------------------
    | Default Config Portonepay
    |--------------------------------------------------------------------------
    |
    | This option defines the default configuration about Portonepay.
    |
    */

return [
    'key' => env('PORTONE_KEY', ''),
    'secret' => env('SECRET_KEY', ''),
    'environment' => env('ENVIRONMENT_MODE', 'sandbox'),
];
