<?php return array(
    'root' => array(
        'name' => 'pacificcross/portonepay',
        'pretty_version' => 'dev-master',
        'version' => 'dev-master',
        'reference' => 'dc09826a75ef2ec4a49708d7d6e0475484a95f31',
        'type' => 'library',
        'install_path' => __DIR__ . '/../../',
        'aliases' => array(),
        'dev' => true,
    ),
    'versions' => array(
        'pacificcross/portonepay' => array(
            'pretty_version' => 'dev-master',
            'version' => 'dev-master',
            'reference' => 'dc09826a75ef2ec4a49708d7d6e0475484a95f31',
            'type' => 'library',
            'install_path' => __DIR__ . '/../../',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
    ),
);
