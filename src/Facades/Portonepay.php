<?php

namespace Pacificcross\Portonepay\Facades;

use Illuminate\Support\Facades\Facade;

class Portonepay extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'portonepay';
    }
}
