<?php

namespace Pacificcross\Portonepay;

use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\ValidationException;

class Portonepay
{
    protected $key;
    protected $secret;
    protected $environment;
    protected $url = 'https://api.portone.cloud/api';
    protected $endpoint;

    protected $smchKey;

    public function __construct()
    {
        $this->key = config('portonepay.key');
        $this->secret = config('portonepay.secret');
        $this->environment = config('portonepay.environment');
    }

    protected function sendRequest(array $data, string $method)
    {
        $token = Helper::generateJWTToken($this->key, $this->secret);

        $headers = [
            'Accept' => 'application/json',
            'Content-Type' => 'application/json',
            'Authorization' => 'Bearer ' . $token,
            'X-Portone-Client-Key' => $this->key,
        ];

        $url = $this->url . $this->endpoint;

        $client = Http::withHeaders($headers);

        $validMethods = ['post', 'put', 'get', 'delete'];

        if (!in_array(strtolower($method), $validMethods)) {
            throw new \InvalidArgumentException('Invalid HTTP method: ' . $method);
        }

        $response = call_user_func([$client, $method], $url, $data);

        return $response->json();
    }

    public function createPaymentLink(array $data)
    {
        $this->endpoint = '/paymentLink';

        try {
            $validatedData = RequestValidator::validateRequestCreatePaymentLink($data);
        } catch (ValidationException $e) {
            return response()->json([
                'status' => 'error',
                'message' => $e->errors()
            ], 422);
        }

        $validatedData['portone_key'] = $this->key;
        $signatureHash = Helper::generateSignatureHash($data, $this->key, $this->secret);
        $validatedData['signature_hash'] = $signatureHash;
        $validatedData['environment'] = $this->environment;

        return $this->sendRequest($validatedData,'post');
    }

    public function createInitiatePayment(array $data)
    {
        $this->endpoint = '/initiatePayment';

        try {
            $validatedData = RequestValidator::validateRequestCreateInitiatePayment($data);
        } catch (ValidationException $e) {
            return response()->json([
                'status' => 'error',
                'message' => $e->errors()
            ], 422);
        }

        $validatedData['key'] = $this->key;
        $signatureHash = Helper::generateSignatureHash($data, $this->key, $this->secret);
        $validatedData['signature_hash'] = $signatureHash;
        $validatedData['environment'] = $this->environment;

        return $this->sendRequest($validatedData,'post');
    }

    public function updatePaymentLink(array $data)
    {
        try {
            $validatedData = RequestValidator::validateRequestUpdatePaymentLink($data);
        } catch (ValidationException $e) {
            return response()->json([
                'status' => 'error',
                'message' => $e->errors()
            ], 422);
        }

        $this->endpoint = '/paymentLink/' . $validatedData['payment_link_ref'] . '/status/' . $validatedData['status'];

        return $this->sendRequest($validatedData, 'put');
    }

    public function getTransactionStatus(array $data)
    {
        try {
            $validatedData = RequestValidator::validateRequestGetTransactionStatus($data);
        } catch (ValidationException $e) {
            return response()->json([
                'status' => 'error',
                'message' => $e->errors()
            ], 422);
        }

        $this->endpoint = '/transaction/' . $validatedData['merchant_order_id'] . '/status';

        return $this->sendRequest($validatedData,'get');
    }

    public function getTransactionDetails(array $data)
    {
        try {
            $validatedData = RequestValidator::validateRequestGetTransactionDetail($data);
        } catch (ValidationException $e) {
            return response()->json([
                'status' => 'error',
                'message' => $e->errors()
            ], 422);
        }

        $this->endpoint = '/'.$this->key.'/transaction-details';

        return $this->sendRequest($validatedData, 'post');
    }

    public function getPaymentLinkStatus(array $data)
    {
        try {
            $validatedData = RequestValidator::validateRequestGetPaymentLinkStatus($data);
        } catch (ValidationException $e) {
            return response()->json([
                'status' => 'error',
                'message' => $e->errors()
            ], 422);
        }

        $this->smchKey = $this->key;

        $this->endpoint =  '/merchant/'. $this->smchKey . '/paymentLink/' . $validatedData['merchant_order_id'] . '/status';

        return $this->sendRequest($validatedData, 'get');
    }
}
