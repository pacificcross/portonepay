<?php

namespace Pacificcross\Portonepay;

use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\ValidationException;

class RequestValidator
{
    public static function validateRequestCreatePaymentLink(array $data)
    {
        $validator = Validator::make($data, [
            'merchant_details.name' => 'required|string',
            'merchant_details.promo_discount' => 'required|numeric',
            'merchant_details.shipping_charges' => 'required|numeric',
            'merchant_order_id' => 'required|string',
            'amount' => 'required|numeric',
            'description' => 'required|string',
            'currency' => 'required|string',
            'country_code' => 'required|string',
            'expiry_date' => 'required|date_format:Y-m-d\TH:i:s.v\Z',
            'source' => 'required|string',
            'success_url' => 'required|url',
            'failure_url' => 'required|url',
            'pending_url' => 'required|url',
        ]);

        if ($validator->fails()) {
            throw new ValidationException($validator);
        }

        return $data;
    }

    public static function validateRequestCreateInitiatePayment(array $data)
    {
        $validator = Validator::make($data, [
            'pmt_channel' => 'required|string',
            'pmt_method' => 'required|string',
            'merchant_order_id' => 'required|string',
            'amount' => 'required|numeric',
            'description' => 'required|string',
            'currency' => 'required|string',
            'country_code' => 'required|string',
            'expiry_date' => 'required|date_format:Y-m-d\TH:i:s.v\Z',
            'source' => 'required|string',
            'success_url' => 'required|url',
            'failure_url' => 'required|url',
            'pending_url' => 'required|url',
        ]);

        if ($validator->fails()) {
            throw new ValidationException($validator);
        }

        return $data;
    }

    public static function validateRequestUpdatePaymentLink(array $data)
    {
        $validator = Validator::make($data, [
            'payment_link_ref' => 'required|string',
            'status' => 'required|string'
        ]);

        if ($validator->fails()) {
            throw new ValidationException($validator);
        }

        return $data;
    }

    public static function validateRequestGetTransactionStatus(array $data)
    {
        $validator = Validator::make($data, [
            'merchant_order_id' => 'required|string'
        ]);

        if ($validator->fails()) {
            throw new ValidationException($validator);
        }

        return $data;
    }

    public static function validateRequestGetTransactionDetail(array $data)
    {
        $validator = Validator::make($data, [
            'order_ref' => 'required|string'
        ]);

        if ($validator->fails()) {
            throw new ValidationException($validator);
        }

        return $data;
    }

    public static function validateRequestGetPaymentLinkStatus(array $data)
    {
        $validator = Validator::make($data, [
            'merchant_order_id' => 'required|string'
        ]);

        if ($validator->fails()) {
            throw new ValidationException($validator);
        }

        return $data;
    }
}
