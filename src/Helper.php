<?php

namespace Pacificcross\Portonepay;

class Helper
{
    public static function generateSignatureHash(array $data, string $portoneKey, string $secretKey)
    {
        $mainParams = http_build_query([
            'amount' => $data['amount'],
            'client_key' => $portoneKey,
            'currency' => $data['currency'],
            'failure_url' => $data['failure_url'],
            'merchant_order_id' => $data['merchant_order_id'],
            'success_url' => $data['success_url'],
        ]);

        $hash = hash_hmac('sha256', $mainParams, $secretKey, true);
        return base64_encode($hash);
    }

    public static function base64url($source)
    {
        $encodedSource = base64_encode($source);
        $encodedSource = rtrim($encodedSource, '=');
        $encodedSource = str_replace(['+', '/'], ['-', '_'], $encodedSource);
        return $encodedSource;
    }

    public static function generateJWTToken($key, $secret_key)
    {

        $header = [
            'alg' => 'HS256',
            'typ' => 'JWT'
        ];
        $stringifiedHeader = json_encode($header);
        $encodedHeader = self::base64url($stringifiedHeader);

        $timeNow = time();

        $data = [
            'iss' => 'PORTONE',
            'sub' => $key,
            'iat' => $timeNow,
            'exp' => $timeNow + 1000
        ];
        $stringifiedData = json_encode($data);
        $encodedData = self::base64url($stringifiedData);

        $token = $encodedHeader . "." . $encodedData;
        $signature = self::base64url(hash_hmac('sha256', $token, $secret_key, true));

        $signedToken = $token . "." . $signature;

        return $signedToken;
    }

}
