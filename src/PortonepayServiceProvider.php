<?php

namespace Pacificcross\Portonepay;

use Illuminate\Support\ServiceProvider;
use Pacificcross\Portonepay\Portonepay;

class PortonepayServiceProvider extends ServiceProvider
{
    public function boot()
    {
        $this->publishes([
            __DIR__ . '/../config/portonepay.php' => config_path('portonepay.php'),
        ]);
    }

    public function register()
    {
        $this->mergeConfigFrom(
            __DIR__ . '/../config/portonepay.php', 'portonepay'
        );

        $this->app->singleton('portonepay', function () {
            return new Portonepay;
        });
    }
}
