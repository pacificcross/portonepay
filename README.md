# PortOnePay - Laravel Packages for PortOne Payment Gateway

PortOnePay is a Laravel package to integrate the PortOne Payment Gateway.

## Installation

Add this package to your Laravel project via Composer:

```bash
composer require pacificcross/portonepay
```

## Publish Configuration

To publish the configuration file, run the following command:

```php
  php artisan vendor:publish --provider="Pacificcross\Portonepay\PortonepayServiceProvider"
```

## Environment Variables

To run this project, you will need to add the following environment variables to your .env file

`PORTONE_KEY` => Your PortOne Key in Setting > API

`SECRET_KEY` => Your PortOne Secret Key in Setting > API

`ENENVIRONMENT_MODE` => Choose `"sandbox"` for testing only or `"live"` for the production.

## Usage/Examples

#### Facades/Aliases

```php
use Pacificcross\Portonepay\Facades\Portonepay;
```

### 1. Payment Link

#### Create Payment Link

```php

    $data = [
        'merchant_details' => [
            'name' => 'Pacific Cross',
            'promo_discount' => 0.0,
            'shipping_charges' => 0
        ],
        'merchant_order_id' => 'TRX0239329343',
        'amount' => 123000,
        'description' => 'Test description',
        'currency' => 'IDR',
        'country_code' => 'ID',
        'expiry_date' => '2025-02-27T14:30:00.000Z',
        'source' => 'salesportal',
        'success_url' => 'https://checkout.portone.cloud/success.html',
        'failure_url' => 'https://checkout.portone.cloud/failure.html',
        'pending_url' => 'https://checkout.portone.cloud/pending.html',
    ];

    $response = Portonepay::createPaymentLink($data);

```
#### Get Payment Link Status

```php

    $data = [
                'merchant_order_id' => 'TRX0239329328',
            ];

    $response = Portonepay::getPaymentLinkStatus($data);
```

#### Update Payment Link Status

```php

    $data = [
            'payment_link_ref' => '2ipEK2hiq6UMWDZshRpcT9HIwt7',
            'status' => 'cancelled',
    ];

    $response = Portonepay::updatePaymentLink($data);

```

## License

[MIT](https://choosealicense.com/licenses/mit/)

## Support

For any question, support and contributing, please contact us on id.development@pacificcross.co.id
